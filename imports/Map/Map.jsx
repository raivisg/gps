import React, { Component, PropTypes } from 'react';
import { GoogleMapLoader, GoogleMap, Marker, InfoWindow } from "react-google-maps";
import { Cars } from '../Admin/AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';

export default class Map extends Component {

	constructor(props) {
	    super(props);

	    this.state = { 
	    	showInfo: false
	    }
  	}

  	toggleInfoWindow(marker) {
  		marker.showInfo = !marker.showInfo;
    	this.setState({
    		showInfo: !marker.showInfo
    	});
  	}

	renderMarkers() {
		return this.props.markers.map((marker) => 
      		<Marker 
      			key={marker._id}
      			position={{lat: parseFloat(marker.lat), lng: parseFloat(marker.lng)}} 
      			title={marker.num}
      			onClick={this.toggleInfoWindow.bind(this, marker)}
      		>
      		{marker.showInfo ? this.renderInfoWindow(marker) : null}
      		</Marker>
    	);
	}

	renderInfoWindow(marker) {
    
    return (
      	<InfoWindow
        	onCloseclick={this.toggleInfoWindow.bind(this, marker)} >
        	<p><strong>{marker.num}</strong><br>
        	</br>{marker.date}<br>
        	</br>{marker.currentAddr}</p>
      	</InfoWindow>
      
    );
    
  }

	render() {
	    return (
	    		<GoogleMapLoader
		         containerElement={<div id="map"/>}
		         googleMapElement={
	         		<GoogleMap
		             defaultZoom={9}
		             defaultCenter={{lat: 56.947810, lng: 24.101005}}
		             mapTypeId={google.maps.MapTypeId.HYBRID}
                 onClick={this.props.hideMenuComponents}
            		>
            		{this.renderMarkers()}
            		</GoogleMap>
          		}/>
			)
	}
}

Map.propTypes = {
  markers: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('cars');
  return {
    markers: Cars.find({checked: true}, {sort: {num: 1}}).fetch()
  };
}, Map);