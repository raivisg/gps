export const Cars = new Mongo.Collection('cars');

if (Meteor.isServer){
	Meteor.publish('users', function usersPublication(){
		if (Roles.userIsInRole(this.userId, 'admin')){
  			return Meteor.users.find();
		}else{
			if(this.userId){
				return Meteor.users.find({_id: this.userId});
			}
		}
  	});

  	Meteor.publish('cars', function carsPublication(){
  		if (Roles.userIsInRole(this.userId, 'admin')){
  			return Cars.find();
  		}else{
  			if(this.userId){
		        return Cars.find({owner: this.userId});
		      }
  		}
  	});

  	Meteor.publish('roles', function rolesPublication(){
  		if (Roles.userIsInRole(this.userId, 'admin')){
  			return Meteor.roles.find();
  		}
  	});
}