import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import User from './User.jsx';
import Group from './Groups.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class EditUserGroups extends Component {

	componentDidMount() {
		$(this.refs.user).dropdown();
		$(this.refs.groups).dropdown();
		$(this.refs.opt).dropdown();
	}

	renderUserNames() {
    	return this.props.users.map((user) => (
      		<User key={user._id} user={user} />
    	));
  	}

  	renderUserGroups() {
    	return this.props.groups.map((group) => (
      		<Group key={group._id} group={group} />
    	));
  	}

	editUserGroups(event){

		event.preventDefault();

  		const group = ReactDOM.findDOMNode(this.refs.groups).value.trim();
		const id = ReactDOM.findDOMNode(this.refs.user).value.trim();
		const option = ReactDOM.findDOMNode(this.refs.opt).value.trim();

		if(id && option && group ){

			if (option == 1) 
				Meteor.call("changeUsersGroups", id, group);
			if (option == 2) 
				Meteor.call("removeUsersGroups", id, group);

	        $(this.refs.user).dropdown('clear');
			$(this.refs.groups).dropdown('clear');
			$(this.refs.opt).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
    	}
  	}

	render() {
    	return (
    		<div id="editUserGroups" className="ap">

			    <h3>Rediģēt lietotāja grupas</h3>

			    <form className="ui form" ref="EditUser" onSubmit={this.editUserGroups.bind(this)}>

				    <select className="ui selection dropdown" ref="user">
				    	<option value="">Lietotājs</option>
				    		{this.renderUserNames()}
				    	</select>

				    <select className="ui selection dropdown" ref="opt">
				    	<option value="">Pievienot/Noņemt</option>
				    	<option value="1">Pievienot grupu</option>
					    <option value="2">Noņemt grupu</option>
				    </select>

				    <select className="ui selection dropdown" ref="groups">
				    	<option value="">Grupa</option>
				    		{this.renderUserGroups()}
				    	</select>

			    <button className="ui button" type="submit">Rediģēt</button>

			    </form>
		  </div>
    	)
    }
}

EditUserGroups.propTypes = {
  users: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
	Meteor.subscribe('roles');
  return {
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch(),
    groups: Meteor.roles.find({}, {sort: {name: 1}}).fetch()
  };
}, EditUserGroups);