import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import User from './User.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class DeleteUser extends Component {

	componentDidMount() {
		$(this.refs.user).dropdown();
	}

	renderUserNames() {
    	return this.props.users.map((user) => (
      		<User key={user._id} user={user} />
    	));
  	}

	editUserProfile(event){

		event.preventDefault();

		const id = ReactDOM.findDOMNode(this.refs.user).value.trim();

		if(id ){
  	  	
	        Meteor.call('removeUser', id);

	        $(this.refs.user).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav izvēlēts lietotājs!", html: true});
    	}
  	}

	render() {
    	return (
    		<div id="editUserInfo" className="ap">

			    <h3>Dzēst lietotāju</h3>

			    <form className="ui form" ref="EditUser" onSubmit={this.editUserProfile.bind(this)}>

				    <select className="ui selection dropdown" ref="user">
				    	<option value="">Lietotājs</option>
				    		{this.renderUserNames()}
				    	</select>

			    <button className="ui button" type="submit">Dzēst</button>

			    </form>
		  </div>
    	)
    }
}

DeleteUser.propTypes = {
  users: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
  return {
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch()
  };
}, DeleteUser);