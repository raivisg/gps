import React, { Component, PropTypes } from 'react';
import Car from './TableCars.jsx';
import { Cars } from './AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';

export default class Table extends Component {

	renderCars(){
    	return this.props.cars.map((car) => (
      		<Car key={car._id} car={car} />
    	));
  	}

	render(){
		return(
				<div id="divider">
					<div id="grid">
						<h3>Transportlīdzekļu saraksts</h3>
				        <table className="ui unstackable inverted table">
						  <thead>
						    <tr className="center aligned">
						      	<th>Numurs</th>
						    	<th>Imei</th>
						    	<th>Īpašnieks</th>
						    	<th className="hideLocation">Tagadējā atrašanās vieta</th>
						    	<th className="hide">Aizdedzes kontrole</th>
						    </tr>
						  </thead>
						  <tbody>
						    {this.renderCars()}
						  </tbody>
						</table>
					</div>
				</div>
			)
	}
}

Table.propTypes = {
  cars: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
	Meteor.subscribe('cars');
  return {
    cars: Cars.find({}, {sort: {num: 1}}).fetch(),
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch()
  };
}, Table);