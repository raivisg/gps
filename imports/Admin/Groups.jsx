import React, { Component, PropTypes } from 'react';

export default class Group extends Component {

  render() {
    return (
      <option value={this.props.group._id}>
      	{this.props.group.name}
      </option>
    );
  }
}

 
Group.propTypes = {
  group: PropTypes.object.isRequired
};