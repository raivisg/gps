import React, { Component, PropTypes } from 'react';
 
export default class User extends Component {

  render() {
    return (
      <option value={this.props.user._id}>
      	{this.props.user.username}
      </option>
    );
  }
}
 
User.propTypes = {
  user: PropTypes.object.isRequired
};