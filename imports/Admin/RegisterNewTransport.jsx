import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import User from './User.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class RegisterNewTransport extends Component {

	componentDidMount() {
		$(this.refs.carOwner).dropdown();
		$(this.refs.carIgnition).dropdown();
	}

	registerTransport(event){

		event.preventDefault();

  		const num = ReactDOM.findDOMNode(this.refs.number).value.trim();
		const imei = ReactDOM.findDOMNode(this.refs.imei).value.trim();
		const owner = ReactDOM.findDOMNode(this.refs.carOwner).value.trim();
		const ignition = ReactDOM.findDOMNode(this.refs.carIgnition).value.trim();

		if (num && imei && owner && ignition){
  			Meteor.call('addCar', num, imei, owner, ignition);
        	ReactDOM.findDOMNode(this.refs.RegCar).reset();
			$(this.refs.carOwner).dropdown('clear');
			$(this.refs.carIgnition).dropdown('clear');
  		}else{
  			swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
  		}

  	}

  	renderUserNames(){
    	return this.props.users.map((user) => (
      		<User key={user._id} user={user} />
    	));
  	}

	render() {
	    return (
	    	<div id="registerCar" className="ap">
		    <h3>Pievienot transportlīdzekli</h3>

		      <form className="ui form car" id="carr" ref="RegCar" onSubmit={this.registerTransport.bind(this)}>

		        <div className="field">
		          <label>Reģistrācijas numurs</label>
		          <input 
		          	type="text"
		          	ref="number"
		          	placeholder="Numurs" 
		          />
		        </div>

		        <div className="field">
		          <label>IMEI kods</label>
		          <input placeholder="IMEI"
			        type="text"
			        ref="imei"
			        placeholder="IMEI"
		          />
		        </div>

		        <div className="field">
		          <label>Piesaistīts</label>
		        </div>
		        <select className="ui selection dropdown" ref="carOwner">
		        	<option value="">Lietotājs</option>
		          	{this.renderUserNames()}
		        </select>

		        <div className="field">
		          <label>Aizdedzes pārbaude</label>
		        </div>
		        <select className="ui selection dropdown" ref="carIgnition">
		          <option value="false">Nav</option>
		          <option value="true">Ir</option>
		        </select>

		      <button className="ui button" type="submit">Pievienot</button>

		      </form>
		  	</div>
	    	)
	}
}

RegisterNewTransport.propTypes = {
  users: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
  return {
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch()
  };
}, RegisterNewTransport);