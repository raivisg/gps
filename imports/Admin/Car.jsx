import React, { Component, PropTypes } from 'react';

export default class Car extends Component {

  render() {
    return (
      <option value={this.props.car._id}>
      	{this.props.car.num}
      </option>
    );
  }
}

 
Car.propTypes = {
  car: PropTypes.object.isRequired
};