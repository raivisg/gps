import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor'

export default class Car extends Component {

	renderUsername(userId) {
		return Meteor.users.findOne({_id: userId}).username;
	}


  render() {

    return ( 
		    <tr className="center aligned">
		    	<td>{this.props.car.num}</td>
		    	<td>{this.props.car.IMEI}</td>
		    	<td>{this.renderUsername(this.props.car.owner)}</td>
		    	<td className="hideLocation">{this.props.car.currentAddr}</td>
		    	<td className="hide">{this.props.car.ic}</td>
		    </tr>
    );
  }
}

 
Car.propTypes = {
  car: PropTypes.object.isRequired
};