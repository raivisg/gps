import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import User from './User.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class EditUserProfile extends Component {

	componentDidMount() {
		$(this.refs.user).dropdown();
		$(this.refs.option).dropdown();
	}

	renderUserNames() {
    	return this.props.users.map((user) => (
      		<User key={user._id} user={user} />
    	));
  	}

	editUserProfile(event){

		event.preventDefault();

  		const type = ReactDOM.findDOMNode(this.refs.option).value.trim();
		const id = ReactDOM.findDOMNode(this.refs.user).value.trim();
		const newValue = ReactDOM.findDOMNode(this.refs.newValue).value.trim();

		if(type && id && newValue){
  	  	
	  	  	if (type == 1 ) 
	        	Meteor.call('changeName', id, newValue);
	      	if (type == 2)
	        	Meteor.call('changeStatus', id, newValue);
	      	if (type == 3)
	        	Meteor.call('changePass', id, newValue);
	      	if (type == 4)
	        	Meteor.call('changeFullname', id, newValue);

	        ReactDOM.findDOMNode(this.refs.EditUser).reset();
	        $(this.refs.user).dropdown('clear');
			$(this.refs.option).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
    	}
  	}

	render() {
    	return (
    		<div id="editUserInfo" className="ap">

			    <h3>Rediģēt lietotāja informāciju</h3>

			    <form className="ui form" ref="EditUser" onSubmit={this.editUserProfile.bind(this)}>

				    <select className="ui selection dropdown" ref="user">
				    	<option value="">Lietotājs</option>
				    		{this.renderUserNames()}
				    	</select>

				    <select className="ui selection dropdown" ref="option">
					    <option value="">Nomainīt</option>
					    <option value="1">Lietotāja vārdu</option>
					    <option value="2">Statusu</option>
					    <option value="3">Paroli</option>
					    <option value="4">Pilno vārdu</option>
				    </select>

				    <div className="field">
					    <label>Nomainīt uz</label>
					    <input 
						    type="text"
						    ref="newValue" 
						    placeholder="Jaunā vērtība"
				    	/>
				    </div>

			    <button className="ui button" type="submit">Rediģēt</button>

			    </form>
		  </div>
    	)
    }
}

EditUserProfile.propTypes = {
  users: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
  return {
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch()
  };
}, EditUserProfile);