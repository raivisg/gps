import React, { Component, PropTypes } from 'react';

export default class User extends Component {

  render() {
    return ( 
		    <tr>
		    	<td>{this.props.user.username}</td>
		    	<td>{this.props.user.profile.fullname}</td>
		    	<td>{this.props.user.profile.status}</td>
		    	<td>{this.props.user.roles+" "}</td>
		    </tr>
    );
  }
}

 
User.propTypes = {
  user: PropTypes.object.isRequired
};