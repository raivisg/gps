import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import AdminMenu from './AdminMenu';
import { Cars } from './AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Table from './Table';

export default class Admin extends Component {

	componentDidMount() {
		$(this.refs.adminMenuOpt).popup();
		$(this.refs.userDropdown).dropdown();
		$(this.refs.transpDropdown).dropdown();
		$(this.refs.tableDropdown).dropdown();
		$(this.refs.groupDropdown).dropdown();
	}

	render() {
	    return (
	    	<div>
	    	{ Roles.userIsInRole(Meteor.user(), 'admin') ?
	    	<div>
	    		<AdminMenu />
	    		<div id="adminWrapper">
			    	<div className="ui four item inverted menu">

					  <div className="ui dropdown item" ref="userDropdown">
					      Lietotāji
					      <i className="dropdown icon"></i>
					      <div className="menu">
					        <a href={FlowRouter.path('admin.RegisterUser')} className="item"><i className="add user icon"></i>Reģistrēt lietotāju</a>
					        <a href={FlowRouter.path('admin.EditUserProfile')} className="item"><i className="edit icon"></i>Rediģēt lietotāja informāciju</a>
					        <a href={FlowRouter.path('admin.EditUserGroups')} className="item"><i className="edit icon"></i>Rediģēt lietotāja grupas</a>
					        <a href={FlowRouter.path('admin.DeleteUser')} className="item"><i className="remove user icon"></i>Dzēst lietotāju</a>
					      </div>
					   </div>

					  <div className="ui dropdown item" ref="transpDropdown">
					      Transports
					      <i className="dropdown icon"></i>
					      <div className="menu">
					        <a href={FlowRouter.path('admin.RegisterNewTransport')} className="item"><i className="write icon"></i>Pievienot transportlīdzekli</a>
					        <a href={FlowRouter.path('admin.EditTransportProfile')} className="item"><i className="edit icon"></i>Rediģēt transportlīdzekli</a>
					        <a href={FlowRouter.path('admin.DeleteTransport')} className="item"><i className="remove circle icon"></i>Dzēst transportlīdzekli</a>
					      </div>
					   </div>

					  <div className="ui dropdown item" ref="tableDropdown">
					      Tabulas
					      <i className="dropdown icon"></i>
					      <div className="menu">
					        <a href={FlowRouter.path('admin.TableUsers')} className="item"><i className="users icon"></i>Lietotāju saraksts</a>
					        <a href={FlowRouter.path('admin.Table')} className="item"><i className="database icon"></i>Transportlīdzekļu saraksts</a>
					      </div>
					   </div>

					  <div className="ui dropdown item" ref="groupDropdown">
					      Grupas
					      <i className="dropdown icon"></i>
					      <div className="menu">
					        <a href={FlowRouter.path('admin.AddGroup')} className="item"><i className="write icon"></i>Pievienot grupu</a>
					        <a href={FlowRouter.path('admin.EditGroup')} className="item"><i className="edit icon"></i>Rediģēt grupu</a>
					        <a href={FlowRouter.path('admin.DeleteGroup')} className="item"><i className="remove icon"></i>Dzēst grupu</a>
					      </div>
					   </div>
				</div>
		            {this.props.content}
				</div>
			</div> : null }
			</div>
			)
	}
}

export default createContainer(() => {
	Meteor.subscribe('cars');
	Meteor.subscribe('users');
  return {
  	cars: Cars.find({}, {sort: {num: 1}}).fetch(),
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch(),

  };
}, Admin);

