import React, { Component, PropTypes } from 'react';
import User from './TableUsers.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class TableUsers extends Component {

	renderUsers(){
    	return this.props.users.map((user) => (
      		<User key={user._id} user={user} />
    	));
  	}

	render(){
		return(
				<div id="divider">
					<div id="grid">
						<h3>Lietotāju saraksts</h3>
				        <table className="ui unstackable inverted table">
						  <thead>
						    <tr>
						      	<th>Lietotāja vārds</th>
							    <th>Pilnais vārds</th>
							    <th>Statuss</th>
							    <th>Lietotāju grupas</th>
						    </tr>
						  </thead>
						  <tbody>
						    	{this.renderUsers()}
						  </tbody>
						</table>
					</div>
				</div>
			)
	}
}

TableUsers.propTypes = {
  users: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
  return {
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch()
  };
}, TableUsers);