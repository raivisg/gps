import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Group from './Groups.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class DeleteGroup extends Component {

	componentDidMount() {
		$(this.refs.groups).dropdown();
	}

	renderRoles(){
    	return this.props.groups.map((group) => (
      		<Group key={group._id} group={group} />
    	));
  	}


  	deleteGroup(event){

  		event.preventDefault();

		const group = ReactDOM.findDOMNode(this.refs.groups).value.trim();

		if (group){

			Meteor.call("removeGroup", group);

			$(this.refs.groups).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav izvēlēta grupa!", html: true});
    	}

  	}


	render(){
		return(
				<div id="deleteGroup" className="ap">
				    <h3>Dzēst grupu</h3>
				    <form className="ui form" ref="deleteGroup" onSubmit={this.deleteGroup.bind(this)}>

					    <select className="ui selection dropdown" ref="groups">
						    <option value="">Grupa</option>
						    {this.renderRoles()}
						</select>

					    <button className="ui button" type="submit">Dzēst</button>

				    </form>
				</div>
			)
	}
}

DeleteGroup.propTypes = {
  groups: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('roles');
  return {
    groups: Meteor.roles.find({}, {sort: {name: 1}}).fetch()
  };
}, DeleteGroup);