import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Car from './Car.jsx';
import { Cars } from './AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';

export default class EditTransportProfile extends Component {

	componentDidMount() {
		$(this.refs.car).dropdown();
		$(this.refs.optionCar).dropdown();
	}

	renderCars(){
    	return this.props.cars.map((car) => (
      		<Car key={car._id} car={car} />
    	));
  	}

  	editCarProfile(event){

  		event.preventDefault();

  		const type = ReactDOM.findDOMNode(this.refs.optionCar).value.trim();
		const id = ReactDOM.findDOMNode(this.refs.car).value.trim();
		const newValue = ReactDOM.findDOMNode(this.refs.newCarValue).value.trim();

		if (type && id && newValue){

			if (type == 1 ) 
	        	Meteor.call('changeNum', id, newValue);
	      	if (type == 2)
	        	Meteor.call('changeIMEI', id, newValue);
	        if (type == 3)
	        	Meteor.call('changeIgnition', id, newValue);
	        
        	ReactDOM.findDOMNode(this.refs.EditTra).reset();
			$(this.refs.car).dropdown('clear');
			$(this.refs.optionCar).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
    	}

  	}


	render(){
		return(
				<div id="editCar" className="ap">
				    <h3>Rediģēt transportlīdzekli</h3>
				    <form className="ui form" ref="EditTra" onSubmit={this.editCarProfile.bind(this)}>

					      <select className="ui selection dropdown" ref="car">
					      	<option value="">Numurs</option>
					        {this.renderCars()}
					      </select>

					      <select className="ui selection dropdown" ref="optionCar">
					        <option value="">Nomainīt</option>
					        <option value="1">Numuru</option>
					        <option value="2">IMEI kodu</option>
					        <option value="3">Aizdedzes pārbaude</option>
					      </select>

					      <div className="field">
					        <label>Nomainīt uz</label>
					        <input 
					          type="text"
					          ref="newCarValue" 
					          placeholder="Jaunā vērtība"
					        />
					      </div>

					      <button className="ui button" type="submit">Rediģēt</button>

				    </form>
				</div>
			)
	}
}

EditTransportProfile.propTypes = {
  cars: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('cars');
  return {
    cars: Cars.find({}, {sort: {num: 1}}).fetch()
  };
}, EditTransportProfile);