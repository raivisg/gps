import { Cars } from './AdminPublications.js';
import { Meteor } from 'meteor/meteor'

if (Meteor.isServer) {
	Meteor.methods({
		'registerUser'(username, fullname, password, repPassword, status, group){
			let id = Accounts.createUser({
				username: username,
				password: password,
				profile: {
					fullname: fullname,
					status: status
				}
			});
			let role = Meteor.roles.findOne({_id: group});
			Roles.addUsersToRoles(id, role.name);
		},
		'changePass'(id, newValue){
			Accounts.setPassword(id, newValue);
		},
		'changeName'(id, newValue){
			Meteor.users.update(
	       		{_id: id}, 
	        	{$set: {username: newValue}} 
	        );
		},
		'changeUsersGroups'(id, groupId){
			let groupName = Meteor.roles.findOne({_id: groupId});
			groupName = groupName.name;
			Roles.addUsersToRoles(id, groupName);
		},
		'removeUsersGroups'(id, groupId){
			let groupName = Meteor.roles.findOne({_id: groupId});
			groupName = groupName.name;
			Roles.removeUsersFromRoles(id, groupName);
		},
		'changeStatus'(id, newValue){
			let user = Meteor.users.findOne({_id: id});
	      Meteor.users.update(
	        {_id: id}, 
	        {$set: {
	        	profile: {
	        	fullname: user.profile.fullname,
				status: newValue
	        	}
	   			}
	    	}
	        );
	    },
	    'changeFullname'(id, newValue){
			let user = Meteor.users.findOne({_id: id});
	      Meteor.users.update(
	        {_id: id}, 
	        {$set: {
	        	profile: {
		        	fullname: newValue,
					status: user.profile.status
	        		}
	   			}
	    	}
	        );
	    },
	    'removeUser'(id){
	    	Meteor.users.remove(id);
	    },
	    'addCar'(num, imei, owner, ignition){
	      Cars.insert({
	        num: num,
	        IMEI: imei,
	        owner: owner,
	        ic: ignition
	      });
	    },
	    'changeNum'(id, change){
	      Cars.update(
	        {_id: id}, 
	        {$set: {num: change}} 
	      );
	    },
	    'changeIMEI'(id, change){
	      Cars.update(
	          {_id: id}, 
	          {$set: {IMEI: change}}
	      );
	    },
	    'changeIgnition'(id, change){
	      Cars.update(
	          {_id: id}, 
	          {$set: {ic: change}}
	      );
	    },
	    'removeCar'(id){
	      Cars.remove(
	        {_id: id}
	      );
	    },
	    'addGroup'(name){
	    	Roles.createRole(name);
	    },
	    'addUserToGroup'(id, name){
	    	Roles.addUsersToRoles(id, name);
	    },
	    'editGroup'(id, newValue){
	    	let roleName = Meteor.roles.findOne({_id: id});
	    	roleName = roleName.name;
	      Meteor.roles.update(
	        {_id: id}, 
	        {$set: { name: newValue } }
	        );
	      let userArray = Meteor.users.find({roles: roleName}).fetch();
	      Roles.removeUsersFromRoles(userArray, roleName);
	      Roles.setUserRoles(userArray, newValue);
	    },
	    'removeGroup'(id){
	    	let roleName = Meteor.roles.findOne({_id: id});
	    	roleName = roleName.name;
	    	let userArray = Meteor.users.find({roles: roleName}).fetch();
		    Roles.removeUsersFromRoles(userArray, roleName);
		    Roles.deleteRole(roleName);
	    }
	});
    Accounts.validateLoginAttempt(function(attemptInfo) {
    if(this.userId)
      if (attemptInfo.user.profile.status == 'active') {
        return true;
      }else{
        return false;
      }
      if (attemptInfo.methodName == 'createUser') 
        return false;

      return true;
    });
}