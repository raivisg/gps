import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Group from './Groups.jsx';
import { createContainer } from 'meteor/react-meteor-data';
import AdminMenu from './AdminMenu';

export default class RegisterUser extends Component {

	componentDidMount() {
		$(this.refs.status).dropdown();
		$(this.refs.group).dropdown();
	}

	renderGroups(){
    	return this.props.groups.map((group) => (
      		<Group key={group._id} group={group} />
    	));
  	}

	registerUser(event) {
		event.preventDefault();

		const username = ReactDOM.findDOMNode(this.refs.username).value.trim();
		const fullname = ReactDOM.findDOMNode(this.refs.fullname).value.trim();
		const password = ReactDOM.findDOMNode(this.refs.password).value.trim();
		const repPassword = ReactDOM.findDOMNode(this.refs.repPassword).value.trim();
		const status = ReactDOM.findDOMNode(this.refs.status).value.trim();
		const group = ReactDOM.findDOMNode(this.refs.group).value.trim();

		if (username && fullname && password && repPassword && status && group){

			if (repPassword != password){
				swal({title: "ERROR!", text:"Nav vienādas paroles!", html: true});
			}else{
				Meteor.call('registerUser', username, fullname, password, repPassword, status, group, (err, res) =>{
					if (err){ 
						swal({title: "ERROR!", text:err.reason, html: true}); 
						return
					}
					ReactDOM.findDOMNode(this.refs.RegUser).reset();
					$(this.refs.status).dropdown('clear');
					$(this.refs.group).dropdown('clear');
				});
			}
		}else{
			swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
		}
  	}

	render() {
	    return (
		           	<div id="registerUser" className="ap">
					    <h3>Reģistrēt lietotāju</h3>
					    <form className="ui form" ref="RegUser" onSubmit={this.registerUser.bind(this)}>

					      <div className="field">
					        <label>Lietotāja vārds</label>
					        <input 
					        	type="text"
					        	ref="username" 
					        	placeholder="Lietotāja vārds"
					        />
					      </div>

					      <div className="field">
					        <label>Pilnais vārds</label>
					        <input 
					        	type="text"
					        	ref="fullname" 
					        	placeholder="Pilnais vārds"
					        />
					      </div>

					      <div className="field">
					        <label>Parole</label>
					        <input
					        	type="password"
					        	ref="password"
					        	placeholder="Parole"
					        />
					      </div>

					      <div className="field">
					        <label>Atkārtot paroli</label>
					        <input 
					        	type="password"
					        	ref="repPassword"
					        	placeholder="Atkārtot paroli"
					        />
					      </div>

					      <select className="ui selection dropdown" ref="status">
					        <option value="">Statuss</option>
					        <option value="active">Aktīvs</option>
					        <option value="inactive">Neaktīvs</option>
					      </select>

					      <select className="ui selection dropdown" ref="group">
					        <option value="">Grupa</option>
					        {this.renderGroups()}
					      </select>


					      <button className="ui button" type="submit">Reģistrēt</button>

					    </form>
			</div>
		)
	}
}

RegisterUser.propTypes = {
  groups: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('roles');
  return {
    groups: Meteor.roles.find({}, {sort: {name: 1}}).fetch()
  };
}, RegisterUser);