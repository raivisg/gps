import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class AddGroup extends Component {


  	addGroup(event){

  		event.preventDefault();

		const name = ReactDOM.findDOMNode(this.refs.newGroup).value.trim();

		if (name){

	        Meteor.call("addGroup", name);
        	ReactDOM.findDOMNode(this.refs.addGroup).reset();

    	}else{
    		swal({title: "ERROR!", text:"Nav ievadīts grupas nosaukums!", html: true});
    	}
  	}


	render(){
		return(
				<div id="addGroup" className="ap">
				    <h3>Pievienot grupu</h3>
				    <form className="ui form" ref="addGroup" onSubmit={this.addGroup.bind(this)}>

				     	<div className="field">
					        <label>Jaunās grupas nosaukums</label>
					        <input 
					          type="text"
					          ref="newGroup" 
					          placeholder="Jaunās grupas nosaukums"
					        />
				        </div>

					    <button className="ui button" type="submit">Pievienot</button>

				    </form>
				</div>
			)
	}
}