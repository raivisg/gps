import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Car from './Car.jsx';
import { Cars } from './AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';

export default class DeleteTransport extends Component {

	componentDidMount() {
		$(this.refs.car).dropdown();
	}

	renderCars() {
    	return this.props.cars.map((car) => (
      		<Car key={car._id} car={car} />
    	));
  	}

  	editCarProfile(event) {

  		event.preventDefault();

		const id = ReactDOM.findDOMNode(this.refs.car).value.trim();

		if (id){

	        Meteor.call('removeCar', id);
	        
        	ReactDOM.findDOMNode(this.refs.EditTra).reset();
			$(this.refs.car).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav izvēlēts transportlīdzeklis!", html: true});
    	}

  	}


	render() {
		return(
				<div id="editCar" className="ap">
				    <h3>Dzēst transportlīdzekli</h3>
				    <form className="ui form" ref="EditTra" onSubmit={this.editCarProfile.bind(this)}>

					      <select className="ui selection dropdown" ref="car">
					      	<option value="">Numurs</option>
					        {this.renderCars()}
					      </select>

					      <button className="ui button" type="submit">Dzēst</button>

				    </form>
				</div>
			)
	}
}

DeleteTransport.propTypes = {
  cars: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('users');
	Meteor.subscribe('cars');
  return {
    cars: Cars.find({}, {sort: {num: 1}}).fetch(),
    users: Meteor.users.find({}, {sort: {username: 1}}).fetch()
  };
}, DeleteTransport);