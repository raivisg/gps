import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Group from './Groups.jsx';
import { createContainer } from 'meteor/react-meteor-data';

export default class EditGroup extends Component {

	componentDidMount() {
		$(this.refs.groups).dropdown();
	}

	renderRoles(){
    	return this.props.groups.map((group) => (
      		<Group key={group._id} group={group} />
    	));
  	}


  	editGroup(event){

  		event.preventDefault();

		const newName = ReactDOM.findDOMNode(this.refs.newGroup).value.trim();
		const group = ReactDOM.findDOMNode(this.refs.groups).value.trim();

		if (newName && group){

			Meteor.call("editGroup",group ,newName);
	        
        	ReactDOM.findDOMNode(this.refs.editGroup).reset();
			$(this.refs.groups).dropdown('clear');

    	}else{
    		swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
    	}

  	}


	render(){
		return(
				<div id="editGroup" className="ap">
				    <h3>Rediģēt grupu</h3>
				    <form className="ui form" ref="editGroup" onSubmit={this.editGroup.bind(this)}>

					    <select className="ui selection dropdown" ref="groups">
						    <option value="">Grupa</option>
						    {this.renderRoles()}
						</select>

				     	<div className="field">
					        <label>Jaunais grupas nosaukums</label>
					        <input 
					          type="text"
					          ref="newGroup" 
					          placeholder="Jaunais grupas nosaukums"
					        />
				        </div>

					    <button className="ui button" type="submit">Rediģēt</button>

				    </form>
				</div>
			)
	}
}

EditGroup.propTypes = {
  groups: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('roles');
  return {
    groups: Meteor.roles.find({}, {sort: {name: 1}}).fetch()
  };
}, EditGroup);