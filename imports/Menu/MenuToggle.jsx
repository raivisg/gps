import React, { Component, PropTypes } from 'react';

export default class MenuToggle extends Component {

	render() {
	    return (
			 <button className="ui secondary button" id="appear" onClick={this.props.toggleMenu}><i className="sidebar icon"></i></button>
			)
	}
}