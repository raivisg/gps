import React, { Component, PropTypes } from 'react';

export default class Car extends Component {

	toggleChecked() {
    	Meteor.call('toggleChecked', this.props.car._id, !this.props.car.checked);
  	}

  render() {
  		const checkedClass = this.props.car.checked ? '' : 'checked';
    return (
	   	<li className={checkedClass} onClick={this.toggleChecked.bind(this)}>
      		<input type="checkbox" readOnly checked={this.props.car.checked} />
      		{this.props.car.num}
      	</li>
    );
  }
}

Car.propTypes = {
  car: PropTypes.object.isRequired
};