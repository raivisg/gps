import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class ChangePassword extends Component {

	changePassword(event) {

		event.preventDefault();

		let oldPass = ReactDOM.findDOMNode(this.refs.oldPass).value.trim();
		let newPass = ReactDOM.findDOMNode(this.refs.newPass).value.trim();
		let repPass = ReactDOM.findDOMNode(this.refs.repNewPass).value.trim();

		if (oldPass && newPass && repPass) {
			if (newPass === repPass) {
				Accounts.changePassword(oldPass, newPass);
				ReactDOM.findDOMNode(this.refs.changePassword).reset();
			}else{
				swal({title: "ERROR!", text:"Pārbaudiet jauno paroli!", html: true});
			}
		}else{
			swal({title: "ERROR!", text:"Aizpildiet visus laukus!", html: true});
		}
	}

	render() {

	    return (
	    	<div id="changePass">
				<h3>Nomainīt paroli</h3>
				<form className="ui form" ref="changePassword" onSubmit={this.changePassword.bind(this)}>

				<div className="field">
					<label>Vecā parole</label>
					<input type="password" ref="oldPass" placeholder="Vecā parole" />
				</div>

				<div className="field">
					<label>Jaunā parole</label>
					<input  type="password" ref="newPass" placeholder="Jaunā parole" />
				</div>

				<div className="field">
					<label>Atkārtot jauno paroli</label>
					<input type="password" ref="repNewPass" placeholder="Atkārtot jauno paroli" />
				</div>

				<button className="ui button" type="submit" id="button">Nomainīt</button>

				</form>
            </div>
			)
	}
}