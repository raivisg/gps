import React, { Component, PropTypes } from 'react';
import Sidebar from './Sidebar.jsx';
import ChangePassword from './ChangePassword.jsx';
import Excel from './Excel.jsx';
import { Meteor } from 'meteor/meteor'

export default class Menu extends Component {

	constructor(props) {
	    super(props);

	    this.state = {
	    	admin: Meteor.user().roles,
	    	currentUser: Meteor.user().username,
	    	sidebarState: false,
	    	changePassState: false,
	    	ExcelState: false
	    }
  	}

  	toggleSidebar(props) {
  		if (!this.props.componentsVisible){
  			this.setState({
		  		sidebarState: true,
		  		changePassState: false,
			    ExcelState: false
  			});
  		}else{
	  		this.setState({
		  		sidebarState: !this.state.sidebarState,
		  		changePassState: false,
			    ExcelState: false
	  		});
  		}
  		this.props.showMenuContents();
  	}

  	toggleChangePass() {
  		if (!this.props.componentsVisible){
	  		this.setState({
	  			changePassState: true,
	  			sidebarState: false,
		    	ExcelState: false
	  		});
	  	}else{
	  		this.setState({
	  			changePassState: !this.state.changePassState,
	  			sidebarState: false,
		    	ExcelState: false
	  		});
	  	}
  		this.props.showMenuContents();
  	}

  	toggleExcel() {
  		if (!this.props.componentsVisible){
	  		this.setState({
	  			ExcelState: true,
	  			changePassState: false,
	  			sidebarState: false
	  		});
	  	}else{
	  		this.setState({
	  			ExcelState: !this.state.ExcelState,
	  			changePassState: false,
	  			sidebarState: false
	  		});
	  	}
  		this.props.showMenuContents();
  	}

	logout() {
		Meteor.logout();
	}

	render() {
	    return (
			<div id="menu">
				<a className="item logout" onClick={this.logout.bind(this)}><i className="sign out icon"></i>Izlogoties</a>
				
				{this.state.admin == 'admin' ? 
					<a href={FlowRouter.path('admin')} className="item" id="panel"><i className="configure icon"></i>ADMIN</a> : null
				}
				
				<a className="item usern"><i className="user icon"></i>{this.state.currentUser}</a>

				<a className="item" id="changePassAppear" onClick={this.toggleChangePass.bind(this)}><i className="privacy icon"></i>Paroles maiņa</a>

				<a className="item" id="sidebarAppear" onClick={this.toggleSidebar.bind(this)}><i className="car icon"></i>Transports</a>

				<a className="item" id="excelAppear" onClick={this.toggleExcel.bind(this)}><i className="file excel outline icon"></i>Atskaites</a>
				
					{ this.state.sidebarState && this.props.componentsVisible ? <Sidebar /> : null }
					{ this.state.changePassState && this.props.componentsVisible ? <ChangePassword /> : null }
					{ this.state.ExcelState && this.props.componentsVisible ? <Excel /> : null }

            </div>
			)
	}
}