import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Car from '../Admin/Car.jsx';
import { Cars } from '../Admin/AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { HTTP } from 'meteor/http'

export default class Excel extends Component {

	constructor(props) {
	    super(props);

	    this.state = { 
	    	startDate: null,
	    	endDate: null,
	    	dimmerVisible: false
	    }
  	}

  	handleStartDate(date) {
	    this.setState({
	    	startDate: date
	    });
  	}

  	handleEndDate(date) {
	    this.setState({
	    	endDate: date
	    });
  	}

  	toggleDimmer(){
  		this.setState({
  			dimmerVisible: !this.state.dimmerVisible
  		})
  	}

	componentDidMount() {
		$(this.refs.plate).dropdown();
	}

  	renderCars(){
    	return this.props.cars.map((car) => (
      		<Car key={car._id} car={car} />
    	));
  	}

  	exportToExcel(event){
  		event.preventDefault();

	    let carID = ReactDOM.findDOMNode(this.refs.plate).value.trim();
	    let startDate;
	    let endDate;

	    if(this.state.startDate)
	    	startDate = this.state.startDate.format('YYYY/MM/DD');
	    
		if(this.state.endDate)
  			endDate = this.state.endDate.format('YYYY/MM/DD');

  		if(startDate && endDate && carID) {
  			this.setState({
  				dimmerVisible: !this.state.dimmerVisible
  			});

  			startDate += ' 00:00:00';
  			endDate += ' 23:59:59';

	  		Meteor.call('export', carID, startDate, endDate, (err, res) => {
	  			if (err) return console.log("Eksporta errors", err);
	  			$('#dlink').attr("href", res)[0].click();
	  			this.setState({
	  				dimmerVisible: !this.state.dimmerVisible
	  			});
	  		});
  		}else{
  			swal({title: "ERROR!", text:"Nav aizpildīti visi lauki!", html: true});
  		}
  	}

	render() {

	    return (
	    	<div id="excel">
                <h3>Izveidot excel failu</h3>
                <form className="ui form" onSubmit={this.exportToExcel.bind(this)}>

					<div className="field">
						<label>Reģistrācijas numurs</label>
					</div>

					<select name="Auto numurs" className="ui selection dropdown" ref="plate" id="plate">
						<option value="">Numurs</option>
						{this.renderCars()}
					</select>

					<div className="field">
						<label>Sākuma datums</label>
						<div className="ui calendar" id="rangestart">
							<div className="ui input left icon">
								<DatePicker
									locale='en-gb'
									dateFormat="YYYY/MM/DD"
									locale='en-gb'
									placeholderText="Izvēlieties sākuma datumu"
									maxDate={moment()}
							        selected={this.state.startDate}
							        startDate={this.state.startDate}
							        endDate={this.state.endDate}
							        onChange={this.handleStartDate.bind(this)} />
							</div>
						</div>
					</div>

					<div className="field">
						<label>Beigu datums</label>
						<div className="ui calendar" id="rangeend">
							<div className="ui input left icon">
								<DatePicker
									locale='en-gb'
									dateFormat="YYYY/MM/DD"
									locale='en-gb'
									placeholderText="Izvēlieties beigu datumu"
									maxDate={moment()}
							        selected={this.state.endDate}
							        startDate={this.state.startDate}
							        endDate={this.state.endDate}
							        onChange={this.handleEndDate.bind(this)} />
							</div>
						</div>
					</div>
					
					<a id="dlink"></a>

					<button className="ui button" type="submit">Saglabāt</button>
                  
                </form> 
                { this.state.dimmerVisible ?
                <div className="ui dimmer" id="dim">
					<div className="ui text loader">Sagatavo atskaiti</div>
				</div>
				: null }
                
            </div>
			)
	}
}

Excel.propTypes = {
  cars: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('cars');
  return {
    cars: Cars.find({}, {sort: {num: 1}}).fetch()
  };
}, Excel);