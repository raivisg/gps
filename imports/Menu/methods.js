import { Meteor } from 'meteor/meteor';
import { Cars } from '../Admin/AdminPublications.js';
import { HTTP } from 'meteor/http'
import moment from 'moment';
export const CarsLocation = new Mongo.Collection('cars_location');


if (Meteor.isServer) {
	Meteor.methods({
		'toggleChecked'(id, checked){
			Cars.update(id, {
	      		$set: { checked: checked }
	   		});
		},
		'export'(carId, beg, end) {
			let num = Cars.findOne({_id: carId});
			num = num.num;
			let query = CarsLocation.find({carId: carId, currentAddr:{$exists: true}, arrdate: {$gte: beg, $lte: end}}, {sort: {arrdate: 1}}).fetch();
			beg = moment(beg, "YYYY/MM/DD HH:mm:ss").format('DD-MM-YYYY');
			end = moment(end, "YYYY/MM/DD HH:mm:ss").format('DD-MM-YYYY');
			try {
				let result = HTTP.post("http://91.135.16.9:8001/",
					{
					data: {
						data: query, 
						filename: num + "-" + beg + "_" + end +".xlsx", 
						savepath: "/opt/gps/atskaites/static/faili/",
						downloadpath: "http://91.135.16.9:8001/static/faili/"
					}, 
					timeout: 2000
					}
				);
				return result.content;
			} catch (e) {
			return false;
			}
		} 
	});
}