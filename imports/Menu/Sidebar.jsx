import React, { Component, PropTypes } from 'react';
import Car from './Car.jsx';
import { Cars } from '../Admin/AdminPublications.js';
import { createContainer } from 'meteor/react-meteor-data';

export default class Sidebar extends Component {

  	renderCars() {
  		return this.props.cars.map((car) => (
      		<Car key={car._id} car={car} />
    	));
  	}

	render() {

	    return (
	    	<div id="side">
	    		<h4>Atzīmējiet, kurus auto attēlot uz kartes.</h4>
          <ul>
	    		 {this.renderCars()}
          </ul>
	    	</div>
			)
	}
}

Sidebar.propTypes = {
  cars: PropTypes.array.isRequired
};

export default createContainer(() => {
	Meteor.subscribe('cars');
  return {
    cars: Cars.find({}, {sort: {num: 1}}).fetch()
  };
}, Sidebar);