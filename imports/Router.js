import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor'
import Gps from '../imports/Gps/Gps.jsx';
import Admin from '../imports/Admin/Admin.jsx';
import RegisterUser from '../imports/Admin/RegisterUser.jsx';
import EditUserProfile from '../imports/Admin/EditUserProfile';
import RegisterNewTransport from '../imports/Admin/RegisterNewTransport';
import EditTransportProfile from '../imports/Admin/EditTransportProfile';
import AddGroup from '../imports/Admin/AddGroups';
import EditGroup from '../imports/Admin/EditGroup';
import DeleteGroup from '../imports/Admin/DeleteGroup';
import Table from '../imports/Admin/Table';
import TableUsers from '../imports/Admin/TableUser';
import EditUserGroups from '../imports/Admin/EditUserGroups';
import DeleteUser from '../imports/Admin/DeleteUser';
import DeleteTransport from '../imports/Admin/DeleteTransport';
import {mount} from 'react-mounter';

FlowRouter.route('/', {
	name: 'main',
    action: function(params, queryParams) {
        mount(Gps);
    }
});

FlowRouter.route('/admin', {
    action: function(params, queryParams) {
        mount(Admin, {content: <RegisterUser />});
    }
});

FlowRouter.route('/false', {
    action: function(params, queryParams) {
        FlowRouter.go('/');
    }
});


let adminRoutes = FlowRouter.group({
  prefix: '/admin',
  name: 'admin'
});

adminRoutes.route('/RegisterUser', {
	name: 'admin.RegisterUser',

  action: function() {
    mount(Admin, {content: <RegisterUser />});
  }
});

adminRoutes.route('/EditUserProfile', {
	name: 'admin.EditUserProfile',

  action: function() {
    mount(Admin, {content: <EditUserProfile />});
  }
});

adminRoutes.route('/EditUserGroups', {
	name: 'admin.EditUserGroups',

  action: function() {
    mount(Admin, {content: <EditUserGroups />});
  }
});

adminRoutes.route('/DeleteUser', {
	name: 'admin.DeleteUser',

  action: function() {
    mount(Admin, {content: <DeleteUser />});
  }
});

adminRoutes.route('/RegisterNewTransport', {
	name: 'admin.RegisterNewTransport',

  action: function() {
    mount(Admin, {content: <RegisterNewTransport />});
  }
});

adminRoutes.route('/EditTransportProfile', {
	name: 'admin.EditTransportProfile',

  action: function() {
    mount(Admin, {content: <EditTransportProfile />});
  }
});

adminRoutes.route('/DeleteTransport', {
	name: 'admin.DeleteTransport',

  action: function() {
    mount(Admin, {content: <DeleteTransport />});
  }
});

adminRoutes.route('/TableUsers', {
	name: 'admin.TableUsers',

  action: function() {
    mount(Admin, {content: <TableUsers />});
  }
});

adminRoutes.route('/Table', {
	name: 'admin.Table',

  action: function() {
    mount(Admin, {content: <Table />});
  }
});

adminRoutes.route('/AddGroup', {
	name: 'admin.AddGroup',

  action: function() {
    mount(Admin, {content: <AddGroup />});
  }
});

adminRoutes.route('/EditGroup', {
	name: 'admin.EditGroup',

  action: function() {
    mount(Admin, {content: <EditGroup />});
  }
});

adminRoutes.route('/DeleteGroup', {
	name: 'admin.DeleteGroup',

  action: function() {
    mount(Admin, {content: <DeleteGroup />});
  }
});