import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import swal from 'sweetalert';

export default class Unauthorized extends Component {

	loginUser(event){
		event.preventDefault();
		const username = ReactDOM.findDOMNode(this.refs.login).value.trim();
		const password = ReactDOM.findDOMNode(this.refs.password).value.trim();
		$(this.refs.logins).form();

		if(username && password) {
			Meteor.loginWithPassword(username, password, function(err){
        	if (err)
           		swal({title: "ERROR!", text:err.reason, html: true});
      		}); 
		}else{
			swal({title: "ERROR!", text:"Ievadiet lietotāja vārdu un paroli!", html: true});
		}
	}

	render() {
	    return (
			<div className="container">
	    		<div id="login">
				    <form className="ui form" ref="logins" onSubmit={this.loginUser.bind(this)}>

				    <div className="field">
				     	<label>Lietotāja vārds</label>
				     	<input 
				      		type="text"
			            	ref="login"
			            	placeholder="Lietotāja vārds"/>
				    </div>

				    <div className="field">
				     	<label>Parole</label>
				     	<input 
				     		type="password"
			            	ref="password"
			            	placeholder="Parole"
			            />
				    </div>

				    <button className="ui submit button" type="submit" id="button">Ielogoties</button>
				    </form>
			    </div>
    		</div>
			)
	}
}