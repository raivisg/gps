import { Meteor } from 'meteor/meteor';
import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import Authorized from '../Authorized.jsx';
import Unauthorized from '../Unauthorized.jsx';

export default class Gps extends Component {

	render() {
	    return (
	    	this.props.currentUser ? <Authorized /> : <Unauthorized />
		)
	}
}

Gps.propTypes = {
  currentUser: PropTypes.object
};

export default createContainer(() => {
  return {
    currentUser: Meteor.user()
  };
}, Gps);