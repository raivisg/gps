import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Menu from '../imports/Menu/Menu.jsx'
import MenuToggle from '../imports/Menu/MenuToggle.jsx'
import Map from '../imports/Map/Map.jsx'

export default class Authorized extends Component {

	constructor(props) {
	    super(props);

	    this.state = { 
	    	menuState: false,
	    	componentsVisible: false
	    }
  	}

  	toggleMenu() {
    	this.setState({
    		menuState: !this.state.menuState 
    	});
  	}

  	showMenuContents() {
  		this.setState({
  			componentsVisible: true
  		});
  	}

  	hideMenuComponents() {
  		this.setState({
  			componentsVisible: false
  		});
  	}


	render() {
	    return (
	    	<div>
	    		{this.state.menuState ? <Menu 
            showMenuContents={this.showMenuContents.bind(this)} 
            componentsVisible={this.state.componentsVisible}/> : null }
				<MenuToggle toggleMenu={this.toggleMenu.bind(this)} />
				<Map hideMenuComponents={this.hideMenuComponents.bind(this)} />
			</div>
			)
	}
}